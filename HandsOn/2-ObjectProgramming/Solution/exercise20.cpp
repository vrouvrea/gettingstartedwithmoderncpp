#include <cmath>
#include <iostream>
#include <sstream>
#include <string>


/************************************/
// Declarations
/************************************/

//! Convenient enum used in \a PrintLine().
enum class RoundToInteger { no, yes };


//! Class in charge of holding numerator and exponent data together.
class PowerOfTwoApprox
{

public:

    PowerOfTwoApprox(int Nbits, double value);

    //! Compute the best possible approximation of `value` with `Nbits`
    //! \return The approximation as a floating point.    
    double Compute(int Nbits, double value);

    //! \return The approximation as a floating point.    
    double AsDouble() const;

    //! Accessor to numerator.
    int GetNumerator() const;

    //! Accessor to exponent.
    int GetExponent() const;

    /*! 
     * \brief Multiply the approximate representation by an integer. 
     * 
     * \param[in] coefficient Integer coefficient by which the object is multiplied.
     * 
     * \return An approximate integer result of the multiplication.
     */
    int Multiply(int coefficient) const;

private:

    int numerator_ {};
    int exponent_ {};
};


//! Class in charge of the display of a given `PowerOfTwoApprox`.
class TestDisplayPowerOfTwoApprox
{
public:
    
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};


//! Class in charge of the display of the sum of 2 `PowerOfTwoApprox` with real coefficients.
class TestDisplaySumOfMultiply
{
public:
    
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const;
    
};


  

//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent);

//! Round `x` to the nearest integer.
int RoundAsInt(double x);

//! Function for error handling. We will see later how to fulfill the same functionality more properly.
//! Don't bother here about [[noreturn]] - it's just a keyword to silence a possible warning telling
//! the program may not return at the calling site (which is definitely the case here as there is a 
//! std::exit() called in the function).
[[noreturn]] void Error(std::string explanation);

//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! We'll see much later that it is typically the kind of function that could be put in an anonymous namespace.
void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator);

//! Maximum integer that might be represented with `nbits` bits.  
int MaxInt(int nbits);



/*!
 * \brief  Print a line with information about error.
 *
 * \param[in] maximum_error_index The error is expressed as an integer over this quantity.
 * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
 * \param[in] optional_string2 String that might appear just before the "[error = ...]".
 * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
 */
void PrintLine(int Nbits, double exact, double approx, int maximum_error_index, 
               RoundToInteger do_round_to_integer,
               std::string optional_string1 = "", std::string optional_string2 = "");


/************************************/
// Definitions
/************************************/

int TimesPowerOf2(int number, int exponent)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

int RoundAsInt(double x)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    return static_cast<int>(std::round(x));
}


[[noreturn]] void Error(std::string explanation)
{
    std::cout << "ERROR: " << explanation << std::endl;
    exit(EXIT_FAILURE);
}


void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = TimesPowerOf2(1, exponent);   
    numerator = RoundAsInt(value * denominator);
}


void TestDisplayPowerOfTwoApprox::Do(int Nbits) const
{
    Display(Nbits, 0.65);
    Display(Nbits, 0.35);    
}


void TestDisplayPowerOfTwoApprox::Display(int Nbits, double value) const
{
    PowerOfTwoApprox approximation(Nbits, value);

    double double_quotient = approximation.AsDouble();
    
    std::ostringstream oconv;
    oconv << " (" << approximation.GetNumerator() << " / 2^" << approximation.GetExponent() << ")";

    PrintLine(Nbits, value, double_quotient, 100, RoundToInteger::no, "", oconv.str());
    // < here you can't use the default argument for `optional_string1` in `PrintLine` declaration,
    // < as there are non default arguments after it.
}


int MaxInt(int nbits)
{ 
    return (TimesPowerOf2(1, nbits) - 1);
}


PowerOfTwoApprox::PowerOfTwoApprox(int Nbits, double value)
{
    int max_numerator = MaxInt(Nbits);
    
    auto& numerator = numerator_; // alias!
    auto& exponent = exponent_; // alias!

    int denominator {};
    
    do
    {
        // I used here the prefix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    // After the while loop we have numerator > max_numerator,  
    // hence we need to update the fraction using the previous exponent with --exponent.
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


double PowerOfTwoApprox::AsDouble() const
{
   int denominator = TimesPowerOf2(1, exponent_);
   return static_cast<double>(GetNumerator()) / denominator;
}


int PowerOfTwoApprox::GetNumerator() const
{
    return numerator_;
}


int PowerOfTwoApprox::GetExponent() const
{
    return exponent_;
}


int PowerOfTwoApprox::Multiply(int coefficient) const
{
    return TimesPowerOf2(GetNumerator() * coefficient, -GetExponent());
}


void TestDisplaySumOfMultiply::Do(int Nbits) const
{
    Display(Nbits, 0.65, 3515, 0.35, 4832);    
}


void TestDisplaySumOfMultiply::Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const
{
    double exact = value1 * coefficient1 + value2 * coefficient2;

    auto approximation1 = PowerOfTwoApprox(Nbits, value1); // auto-to-stick syntax for constructor
    auto approximation2 = PowerOfTwoApprox(Nbits, value2);

    int computed_approx = approximation1.Multiply(coefficient1) + approximation2.Multiply(coefficient2);

    std::ostringstream oconv;
    oconv << value1 << " * " << coefficient1 
        << " + " << value2 << " * " << coefficient2 << " = ";

    PrintLine(Nbits, exact, computed_approx, 1000, RoundToInteger::yes, oconv.str());
    // < here we use the default value for the 6-th argument `optional_string2`
}


void PrintLine(int Nbits, double exact, double approx, int maximum_error_index, 
               RoundToInteger do_round_to_integer,
               std::string optional_string1, std::string optional_string2)            
{
    int error = RoundAsInt(maximum_error_index * std::fabs(exact - approx) / exact);
    
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? RoundAsInt(exact) : exact) << " ~ " << approx
        << optional_string2
        << "  [error = " << error << "/" << maximum_error_index << "]" 
        << std::endl;    
}



/************************************/
// Main function
/************************************/

// [[maybe_unused]] is a C++ 17 keyword to indicate we're fully aware the variable may not be used.
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{     
    TestDisplayPowerOfTwoApprox test_display_approx;
     
    for (int nbits = 2; nbits <= 8; nbits += 2)
         test_display_approx.Do(nbits); 

    std::cout << std::endl;

    TestDisplaySumOfMultiply test_display_sum_of_multiply;

    for (int nbits = 1; nbits <= 8; ++nbits)
        test_display_sum_of_multiply.Do(nbits);

    return EXIT_SUCCESS;
}

