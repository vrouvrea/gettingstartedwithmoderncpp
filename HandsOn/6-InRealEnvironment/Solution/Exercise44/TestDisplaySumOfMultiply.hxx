#pragma once




template<typename IntT>
TestDisplaySumOfMultiply<IntT>::TestDisplaySumOfMultiply(int resolution)
: TestDisplay(resolution)
{ }

template<typename IntT>
TestDisplaySumOfMultiply<IntT>::~TestDisplaySumOfMultiply() = default;



template<typename IntT>
void TestDisplaySumOfMultiply<IntT>::operator()(int Nbits) const
{
    Display(Nbits, 0.65, 3515, 0.35, 4832);    
}


template<typename IntT>
void TestDisplaySumOfMultiply<IntT>::Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const
{
    try 
    {
        double exact = value1 * static_cast<double>(coefficient1) + value2 * static_cast<double>(coefficient2);

        auto approximation1 = PowerOfTwoApprox<IntT>(Nbits, value1); // auto-to-stick syntax for constructor
        auto approximation2 = PowerOfTwoApprox<IntT>(Nbits, value2);

        IntT part1 = approximation1 * coefficient1;
        IntT part2 = coefficient2 * approximation2;

        IntT computed_approx;

        if (__builtin_add_overflow(part1, part2, &computed_approx))
            throw Exception("Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))");

        std::ostringstream oconv;
        oconv << value1 << " * " << coefficient1 
            << " + " << value2 << " * " << coefficient2 << " = ";

        PrintLine<IntT>(Nbits, exact, static_cast<double>(computed_approx), RoundToInteger::yes, oconv.str());
        // < here we use the default value for the 6-th argument `optional_string2`
    }
    catch(const Exception& e)
    {
        PrintOverflow(Nbits, e);
    }
}
