#pragma once

#include <exception>
#include <string>

class Exception : public std::exception
{
public:

    //! Constructor.
    Exception(const std::string& message);

    //! Overrides what method.
    virtual const char* what() const noexcept override;
    
private:
    
    const std::string message_;
};

