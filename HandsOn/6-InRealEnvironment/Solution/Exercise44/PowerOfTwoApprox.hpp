#pragma once

#include <sstream>


//! Class in charge of holding numerator and exponent data together.
template<typename IntT>
class PowerOfTwoApprox
{

public:

    PowerOfTwoApprox(int Nbits, double value);

    //! Compute the best possible approximation of `value` with `Nbits`
    //! \return The approximation as a floating point.    
    double Compute(int Nbits, double value);

    //! Operator for explicit cast to double.
    explicit operator double() const;

    //! Accessor to numerator.
    IntT GetNumerator() const;

    //! Accessor to exponent.
    int GetExponent() const;

private:

    IntT numerator_ {};
    int exponent_ {};
};

/*! 
* \brief Multiply the approximate representation by an integer. 
* 
* \param[in] coefficient Integer coefficient by which the object is multiplied.
* 
* \return An approximate integer result of the multiplication.
*/
template<typename IntT>
IntT operator*(IntT coefficient, const PowerOfTwoApprox<IntT>& approx);

//! To enable commutation for operator*
template<typename IntT>
IntT operator*(const PowerOfTwoApprox<IntT>& approx, IntT coefficient);

//! Operator<< for `PowerOfTwoApprox`
template<typename IntT>
std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation);

//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! We'll see much later that it is typically the kind of function that could be put in an anonymous namespace.
template<typename IntT>
void HelperComputePowerOf2Approx(double value, int exponent, IntT& numerator, IntT& denominator);


#include "PowerOfTwoApprox.hxx"
