Currently CI is used for three kind of jobs:

- Checking all the Jupyter cells are not executed. If you follow the instructions [here](./CONTRIBUTING.md) it should already be ok, but it's checked nonetheless in CI in case pre-commit was not properly activated.
- Generation of three Docker images:
    * One to run the notebooks
    * One with a Fedora environment that may be used to run hands-on.
    * One in use for a demonstration.
- Check all the hands-on solutions compile properly.

Each Docker image is generated if one of two conditions are met:
    * Dockerfile has been changed.
    * A `git tag` has been provided.

The image is generated but is pushed on the registry *only* if a `git tag` is specified.

So to make the CI work you should first do something like:

```shell
git tag -a v24.03 -m "Release from March 2024""
git push mine --tags
```

where `mine` is the name of the remote on Gitlab (replace with the name you use for your Gitlab remote, which might even be `origin` if you clone your fork directly - see [CONTRIBUTING file](./CONTRIBUTING.md)).
