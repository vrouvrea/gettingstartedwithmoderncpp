#include <cstdlib>
#include "foo.hpp"
#include "bar.hpp" // Compilation error: "foo.hpp" is sneakily included here as well!

int main()
{
    Bar bar;
    return EXIT_SUCCESS;
}