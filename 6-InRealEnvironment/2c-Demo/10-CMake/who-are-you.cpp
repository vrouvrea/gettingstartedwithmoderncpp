#include <iostream>
#include "who-are-you.hpp"

std::string WhoAreYou()
{
    std::string name;
    std::cout << "What's your name? ";
    std::cin >> name;
    return name;
}